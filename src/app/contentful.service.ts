import { Injectable } from '@angular/core';

import { createClient,Entry } from 'contentful';
import { from } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const CONFIG = environment.contentful_config;

@Injectable({
  providedIn: 'root'
})
export class ContentfulService {

  private client = createClient({
    space: CONFIG.space,
    accessToken: CONFIG.accessToken
  });

  constructor() { }

  getCourses(query?: object): Promise<Entry<any>[]> {
    console.log(this.client)
    return this.client.getEntries(Object.assign({
      content_type: 'toolkit'
    }, query)
    )
      .then(res => res.items);
  }
  getCourse(courseId): Promise<Entry<any>> {
    return this.client.getEntries(Object.assign({
     content_type: 'blogPost'
    }, {'sys.id': courseId}))
      .then(res => res.items[0]);
  }
}
