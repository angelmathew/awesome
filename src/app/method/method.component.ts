import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-method',
  templateUrl: './method.component.html',
  styleUrls: ['./method.component.scss']
})
export class MethodComponent implements OnInit {
public methods_heading
  constructor() { }

  ngOnInit() {
    this.methods_heading=["Step #1","Step #2","Step #3","Step #4","Step #5","Step #6"];
  }

}
