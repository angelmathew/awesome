import { Component, OnInit } from '@angular/core';
import { ContentfulService } from 'src/app/contentful.service';
import { Entry } from 'contentful';
import {Router} from '@angular/router'
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
public trending_search;
public situation;
public jobtype;
public problem_solving;
products: Entry<any>[] = [];
  constructor(private cs: ContentfulService, private router:Router) { }

  ngOnInit() {
    this.trending_search=["Stakeholder Engagement","Teamwork","Collaboration","Prototyping"];
    this.situation=["Understand your users","Create better ideas","Solve the right problem","Help your team see the bigger picture"]
    this.jobtype=["Strategy","Projects","Operations","People Leadership"]
    this.problem_solving=["Explore","Understand","Deliver"]
    // this.cs.getCourses().then(products => 
    //   {this.products = products
    //     console.log(this.products)})
  }
  onSituationButtonClicked(){
    this.router.navigate(['/situation']
    //{
     // queryParams:{i=5}
    //}
    )
  }

}
