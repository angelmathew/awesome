import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {
public methods_heading;
  constructor(private router:Router) { }

  ngOnInit() {
    this.methods_heading=["Brainwriting","Affinity Mapping","Dot Voting"];
  }
  onMethodClicked()
        {
          this.router.navigate(['/method'],)
         
        }

}
