import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { RightSectionComponent } from './right-section/right-section.component';
import { SituationPageComponent } from './situation-page/situation-page.component';
import { Routes, RouterModule } from '@angular/router';
import { LinksSectionComponent } from './links-section/links-section.component';
import { RecipeComponent } from './recipe/recipe.component';
import { MethodComponent } from './method/method.component';

//const appRoutes:Routes=[{path:"situation",component:SituationPageComponent},{path:"home",component:MainComponent},{path:"",redirectTo:"/home"}];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    RightSectionComponent,
    SituationPageComponent,
    LinksSectionComponent,
    RecipeComponent,
    MethodComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
   // RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
 
})

export class AppModule { }
