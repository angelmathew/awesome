import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { RightSectionComponent } from './right-section/right-section.component';
import { SituationPageComponent } from './situation-page/situation-page.component';
import { RecipeComponent } from './recipe/recipe.component';
import {MethodComponent} from './method/method.component'

import { LinksSectionComponent } from './links-section/links-section.component';



const routes: Routes = [
//{path:"",redirectTo:"/home",pathMatch:"full"},
{path:"",component:MainComponent},
{path:"situation",component:SituationPageComponent},
{path:"recipe",component:RecipeComponent},
{path:"method",component:MethodComponent}
//{path:"situation/:id",component:SituationPageComponent},
//{path:"recipe",component:RecipeComponent}

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
