import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SituationPageComponent } from './situation-page.component';

describe('SituationPageComponent', () => {
  let component: SituationPageComponent;
  let fixture: ComponentFixture<SituationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SituationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SituationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
