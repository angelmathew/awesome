import { Component, OnInit } from '@angular/core';
import { ContentfulService } from 'src/app/contentful.service';
import { Entry } from 'contentful';
import {ActivatedRoute, Params, Router} from '@angular/router'
@Component({
  selector: 'app-situation-page',
  templateUrl: './situation-page.component.html',
  styleUrls: ['./situation-page.component.scss']
})
export class SituationPageComponent implements OnInit {
  public recipe_heading;
  products: Entry<any>[] = [];
  id:number

  constructor(private cs: ContentfulService,private route:ActivatedRoute,private router:Router) { }

  ngOnInit() {
    this.recipe_heading=["From problem to plan in 1 hour","Revitalizing stakeholders","Create ideas with consensus"];
    this.cs.getCourses().then(products => 
      {this.products = products
        console.log(this.products)})
       
  
      
  }
  
  onRecipeClicked()
        {
          this.router.navigate(['/recipe'],)
         
        }
        

}
